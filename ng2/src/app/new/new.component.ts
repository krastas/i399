import { Component, OnInit } from '@angular/core';
import {NameService} from "../name.service";
import {Name} from "../name";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new',
  templateUrl: 'new.html',
  styles: []
})
export class NewComponent implements OnInit {
public newItemTitle;
public newItemNumber;
public id: number;
public name = new Name(this.newItemTitle,this.newItemNumber);

    constructor (private nameService: NameService,
                private router: Router) {

    }
    addNewName() {
        if (this.id != undefined) {
            this.nameService.changeName(name)
                .then(() => {
                    this.router.navigateByUrl('/search');
                });
        }
        else {
            this.nameService.insertName(new Name(this.newItemTitle, this.newItemNumber))
                .then(() => {
                    this.router.navigateByUrl('/search');
                });
        }
    }

  ngOnInit() {

  }

}
