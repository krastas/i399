import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Name} from "./name";

@Injectable()
export class NameService {
  constructor(private http: HttpClient) { }

    private headers = new Headers({ 'Content-Type': 'application/json' });
    private namesUrl = '/names';

  public getNames(): Promise <Name[]> {
      return this.http.get('/names')
          .toPromise()
          .then(result => <Name[]> result);
  }

  public insertName(name: Name): Promise<void> {
      return this.http
          .post('/names', name)
          .toPromise()
          .then(() => <void> null);
  }
    public changeName(id: number): Promise<void> {
        return this.http
            .put('/names', id)
            .toPromise()
            .then(() => <void> null);
    }


    deleteName(name: Name): Promise<void> {
        const url = `${this.namesUrl}/${name._id}`;
        return this.http.delete(url)
            .toPromise()
            .then(() => <void> null)
    }

}
