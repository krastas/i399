export class Name {
    public done: boolean = false;
    public title: string;
    public number: string;
    public _id: number;

    constructor(title:string, number:string) {
        this.title = title;
        this.number = number;
    }
}
