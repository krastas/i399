import { Component, OnInit } from '@angular/core';
import {Name} from "../name";
import {NameService} from "../name.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-search',
  templateUrl: 'search.html',
  styles: []
})

export class SearchComponent implements OnInit {
selectedName: Name;

    constructor (private nameService: NameService,) {
        this.getData(),
        this.deleteName(name);
    }

    names: Name[] = [];

    getData() {
        this.nameService.getNames()
            .then(names => this.names = names);
    }
    deleteName(name: Name): void {
        this.nameService
            .deleteName(name)
            .then(() => {
                this.names = this.names.filter(n => n !== name);
                if (this.selectedName === name) { this.selectedName = null; }
            });
    }

  ngOnInit() {
      this.getData();
  }
}
