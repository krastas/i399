(function() {
    'use strict';

    angular.module('app').controller('SearchCtrl', Ctrl);
    function Ctrl($http, modalService) {
        var vm = this;
        this.removeItem = removeItem;

        init();
        function init() {
            $http.get('api/contacts').then(function (result) {
                vm.contacts = result.data;
            });
        }

        function removeItem(id) {
            modalService.confirm().then(function () {
                return $http.delete('api/contacts/' + id);
        }).then (init);
        }

    }
})();