(function() {
    'use strict';

    angular.module('app').controller('NewCtrl', Ctrl);

    function Ctrl($http, $location, $routeParams) {
        var vm = this;
        var contactId = $routeParams.id;

            this.newName = this.name;
            this.newPhone = '';
            this.getContacts = getContacts;


        this.addNew = addNew;

        getContacts();
        function getContacts() {
            $http.get('api/contacts/' + contactId).then(function (value) {
                var contact = value.data;
                vm.newName = contact.name
                vm.newPhone = contact.phone
            })
        }

        function addNew() {
            if (contactId != undefined) {


                $http.put('api/contacts/' + contactId, {name: this.newName, phone: this.newPhone, done: false, _id: contactId }).then(function () {
                    $location.path('/search');
                });
            }
            else {
                var newItem = {
                    name: this.newName,
                    phone: this.newPhone,
                    done: false
                };

                $http.post('api/contacts', newItem).then(function () {
                    $location.path('/search');
                });
            }

        }






    }

})();