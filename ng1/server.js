'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const Dao = require('./dao.js');
const app = express();

var url ='mongodb://user2:123456@ds151068.mlab.com:51068/i399'
var dao = new Dao();

dao.connect(url).then(() => app.listen(3000));

app.use(bodyParser.json());
app.get('/api/contacts', getContacts);
app.post('/api/contacts', saveContact);
app.put('/api/contacts/:id', updateContact);
app.get('/api/contacts/:id', getContact);
app.delete('/api/contacts/:id', deleteContact);
app.post('/api/contacts/delete', deleteContacts);
app.use(express.static('./'));

function saveContact(request, response) {
    dao.saveContact(request.body)
        .then(() => {
            response.end();
        }).catch(error=> {
            response.json({ error: error});
    });
}

function getContacts(request, response) {
    dao.findAll()
        .then(data => {
        response.json(data);
    }).catch(error => {
        response.json({error: error});
    });
}

function getContact(request, response) {
    var id = request.params.id;
    dao.findOne(id)
        .then(data => {
        response.json(data);
    }).catch(error => {
        response.json({error: error});
    });
}

function deleteContact(request, response) {
    var id = request.params.id;
    dao.deleteContact(id)
        .then(() => {
            response.end();
        }).catch(error=> {
            response.json({ error: error});
    });
}

function deleteContacts(request, response) {
    var contactIds = request.body;
    dao.deleteContacts(contactIds)
        .then(() => {
            response.end();
        }).catch(error=> {
        response.json({ error: error});
    });
}


function updateContact(request, response) {
    var id = request.params.id;
    var contact = request.body;
    dao.update(id, contact).then(data => {
        response.json(data);
    }).catch(error => {
        console.log(error);
        response.end('error' + error);
    });
}