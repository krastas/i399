'use strict';

const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;
const COLLECTION = 'test-collection';



class Dao {

    connect(url) {
        return mongodb.MongoClient.connect(url)
            .then(db => this.db = db);
    }

    findAll() {
        return this.db.collection(COLLECTION).find().toArray();
    }

    findOne(id) {
      id = new ObjectID(id);
      return this.db.collection(COLLECTION).findOne({_id: id});
    }

    update(id, data) {
        id = new ObjectID(id);
        data._id = id;
        return this.db.collection(COLLECTION)
            .updateOne({ _id: id}, data);
    }

    saveContact(data) {
        return this.db.collection(COLLECTION)
            .insertOne(data);
    }

    deleteContact(id) {
        id = new ObjectID(id);
        return this.db.collection(COLLECTION)
            .deleteOne({_id: id});
    }


    deleteContacts(idList) {
        var ids = [];
        for (let id of idList) {
            ids.push(new ObjectID(id))
        }
        return this.db.collection(COLLECTION)
            .deleteMany({_id: {$in: ids}})
    }

    close() {
        if (this.db) {
            this.db.close();
        }
    }
}

module.exports = Dao;